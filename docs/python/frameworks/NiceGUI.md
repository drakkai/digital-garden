# NiceGUI

## Concept 
Page > layouts > components

**Components** (elements):
- buttons
- sliders
- text
- images
- charts

Component triggers **event**/action

Component are linked to **model** (data object) - auto update UI with value changes

**Layouts**:
- grids
- tabs
- carousels
- expansions
- menus

