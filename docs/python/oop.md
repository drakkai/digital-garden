# Classes and Object-orientation
## Class Attributes, Methods, and Properties
### Class attributes 
```python
class MyClass:

    # Define class attributes in the class block
    my_class_attribute = "class attributes go here"
    MY_CONSTANT = "they are often class-specific contants"

    def __init__(self):
        self.my_instance_attribute = "instance attributes here"
```
```python
class ShippingContainer:

    next_serial = 1337

    def __init__(self, owner_code, contents):
        self.owner_code = owner_code
        self.contents = contents
        self.serial = ShippingContainer.next_serial
        ShippingContainer.next_serial += 1
```
Instance attributes | Class attributes
---|---
owner_code | next_serial
contents |
serial |


### Static methods
```python
class ShippingContainer:

    next_serial = 1337

    @staticmethod
    def _generate_serial():
        result = ShippingContainer.next_serial
        ShippingContainer.next_serial += 1
        return result

    def __init__(self, owner_code, contents):
        self.owner_code = owner_code
        self.contents = contents
        self.serial = ShippingContainer._generate_serial()
```
```python
>>> from shipping import *
>>> c1 = ShippingContainer("YML", ["books"])
>>> c1.serial
1337
>>> ShippingContainer.next_serial
1338
```
### Class methods
```python
class MyClass:

    attribute = "class attribute"

    @classmethod
    def my_class_method(cls, message):
        cls.attribute = message
```
```python
class ShippingContainer:

    next_serial = 1337

    @classmethod
    def _generate_serial(cls):
        result = cls.next_serial
        cls.next_serial += 1
        return result

    def __init__(self, owner_code, contents):
        self.owner_code = owner_code
        self.contents = contents
        self.serial = ShippingContainer._generate_serial()
```

### Static methods with inheritance
For polymorphic dispatch invoke static methods through self.
```python
[...]
class RefrigeratedShippingContainer(ShippingContainer):

    @staticmethod
    def _make_bic_code(owner_code, serial):
        return iso6346.create(
            owner_code=owner_code,
            serial=str(serial).zfill(6),
            category="R"
        )
```

### Class methods with inheritance
Use `**kwargs` to thread arguments through named-constructor class-methods to more specialized sublasses.
```python
class ShippingContainer:
[...]
    @classmethod
    def create_empty(cls, owner_code, **kwargs):
        return cls(owner_code, contents=[], **kwargs)

    @classmethod
    def create_with_items(cls, owner_code, items, **kwargs):
        return cls(owner_code, contents=list(items), **kwargs)
[...]
class RefrigeratedShippingContainer(ShippingContainer):

    MAX_CELSIUS = 4.0

    def __init__(self, owner_code, contents, *, celsius, **kwargs):
        super().__init__(owner_code, contents, **kwargs)
        if celsius > RefrigeratedShippingContainer.MAX_CELSIUS:
            raise ValueError("Temperature too hot!")
        self.celsius = celsius
[...]
```
`create_empty`, `create_with_items` - named-constructors  

### Allowed attributes
```python
class A:
	__slots__ = ["_atr1", "_atr2"]

	def __init__(self, atr1, atr2):
		self._atr1 = atr1
		self._art2 = atr2
```

### Getter / setter
```python
class Human:
    __slots__ = ['_name',
                 '_last_name']

    def _set_full_name(self, full_name):
        print('setter')
        self._name, self._last_name = full_name.split()

    def _get_full_name(self):
        print('getter')
        return f'{self._name} {self._last_name}'

    full_name = property(fset=_set_full_name, fget=_get_full_name)

    def __init__(self, full_name):
        self.full_name = full_name


# with decorator
class Human:
    __slots__ = ['_name',
                 '_last_name']

    @property
    def _get_full_name(self):
        print('getter')
        return f'{self._name} {self._last_name}'
	
	@full_name.setter
    def full_name(self, full_name):
        print('setter')
        self._name, self._last_name = full_name.split()

    def __init__(self, full_name):
        self.full_name = full_name
```
## Metaclasses
```python
class Meta(type):

    @classmethod
    def __prepare__(mcs, name, bases, **kwargs):
        print()
        print(f'  Meta.__prepare__({mcs=}, {name=}, {bases=}, {kwargs=})')
        return {'atr5': 99}

    def __new__(mcs, name, bases, attrs, **kwargs):
        print()
        print(f'  Meta.__new__({mcs=}, {name=}, {bases=}, {attrs=}, {kwargs=})')
        return super().__new__(mcs, name, bases, attrs)

    def __init__(cls, name, bases, attrs, **kwargs):
        print()
        print(f'  Meta.__init__({cls=}, {name=}, {bases=}, {attrs=}, {kwargs=})')
        return super().__init__(name, bases, attrs)

    def __call__(cls, *args, **kwargs):
        print()
        print(f'  Meta.__call__({cls=}, {args=}, {kwargs=})')
        return super().__call__(*args, **kwargs)


class A(metaclass=Meta, suffix='_get', akuku=99):
    cls_atr1 = 9

    def m1(self, x, y):
        print('m1', x, y)

    def __new__(cls, *args, **kwargs):
        print()
        print(f'  Class.__new__({cls=}, {args=}, {kwargs=})')
        return super().__new__(cls)

    def __init__(self, *args, **kwargs):
        print()
        print(f'  Class.__init__({self=}, {args=}, {kwargs=})')
```
### Metaclass with function
```python
def metaclass_func(cls_name: str, inh_dict, cls_dict: dict):
    new_cls_dict = {}
    for atr_name, atr_val in cls_dict.items():
        new_cls_dict[atr_name] = atr_val
        if atr_name.startswith("_") and not atr_name.startswith("__"):
            new_cls_dict[f"get{atr_name}"] = lambda self, atr_name=atr_name: getattr(
                self, atr_name
            )

    return type(cls_name, inh_dict, new_cls_dict)


class MyClass(metaclass=metaclass_func):
    _atr1 = 99
    _atr2 = "mama"


a = MyClass()
print(a.get_atr1())
print(a.get_atr2())
```
## Object life cycle
1. `__new__` - run before initialization, return `self`
2. `__init__` - initializes object
3. `__del--` - destructs object
