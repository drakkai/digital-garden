# Descriptors
```python
class MyDescriptor:
    def __init__(self, atr_name):
        self.atr_name = atr_name

    def __set__(self, instance, value):
        print(self, instance, value)
        setattr(instance, self.atr_name, value)

    def __get__(self, instance, owner):
        print(self, instance, owner)
        return getattr(instance, self.atr_name)


class A:
    __slots__ = ['_atr1']
    atr1 = MyDescriptor('_atr1')


if __name__ == '__main__':
    a = A()
    a.atr1 = 999
    print(a.atr1)
```