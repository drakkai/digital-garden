# Threads and multiprocessing
## Threading
```python
import threading
def f1(n):
    print(f'{threading.current_thread().name}, sleeping 3 s\n', end='')
    time.sleep(n)
    print(f'{threading.current_thread().name}, exiting...\n', end='')
    d[threading.current_thread().name] = 'result'


if __name__ == '__main__':
    d = {}
    t1 = threading.Thread(target=f1, args=(3,), daemon=False)
    t2 = threading.Thread(target=f1, args=(3,), daemon=False)
    t3 = threading.Thread(target=f1, args=(3,), daemon=False)

    t1.start()
    t2.start()
    t3.start()

    t1.join()
    t2.join()
    t3.join()
    print(d)
```
## Multiprocessing
### `multiprocessing.Process`
```python
import multiprocessing
import timeit

def fibo(n):
    x1 = 1
    x2 = 1
    for _ in range(n - 1):
        x1, x2 = x2, x1 + x2
    return x2

def n_times_fibo(m):
    for i in range(m):
        fibo(20_000)

def run_parallell(f, total_number, no_of_processes):
    no_opers = total_number // no_of_processes
    list_of_processes = []
    for _ in range(no_of_processes):
        list_of_processes.append(multiprocessing.Process(target=f, args=(no_opers,)))
    for t in list_of_processes:
        t.start()
    for t in list_of_processes:
        t.join()


if __name__ == '__main__':
    for i in [4, 8, 16, 32]:
        t = timeit.timeit(stmt=f'run_parallell(n_times_fibo, 24000, {i})', number=1, globals=globals())
        print(f'{i} : {t}')
```
### `multiprocessing.Manager()`
```python
import multiprocessing

def fibo(n):
    x1 = 1
    x2 = 1
    for _ in range(n - 1):
        x1, x2 = x2, x1 + x2
    return x2


def n_times_fibo(m, d):
    for i in range(m):
        fibo(20_000)
    d[multiprocessing.current_process().name] = f'{multiprocessing.current_process().name} result.'
    return f'{multiprocessing.current_process().name} result.'


if __name__ == '__main__':
    with multiprocessing.Manager() as m:
        d = m.dict()
        p1 = multiprocessing.Process(target=n_times_fibo, args=(100, d))
        p2 = multiprocessing.Process(target=n_times_fibo, args=(100, d))
        p3 = multiprocessing.Process(target=n_times_fibo, args=(100, d))
        p1.start()
        p2.start()
        p3.start()
        p1.join()
        p2.join()
        p3.join()
        print(d)
```
### `multiprocessing.Pool()`
```python
import multiprocessing
import time

def fibo(n):
    x1 = 1
    x2 = 1
    for _ in range(n - 1):
        x1, x2 = x2, x1 + x2
    return x2

def n_times_fibo(m):
    for i in range(m):
        fibo(20_000)
    return f'{multiprocessing.current_process().name} result.'

if __name__ == "__main__":
    with multiprocessing.Pool(16) as pool:
        # r = pool.apply_async(n_times_fibo, (500,))
        # print(r)
        # while not r.ready():
        #     print('Waiting...')
        #     time.sleep(1)
        # print(r.get())
        # r = pool.map(n_times_fibo, [100] * 100)
        # print(r)
        # r = pool.map_async(n_times_fibo, [100] * 100)
        # print(r)
        # while not r.ready():
        #     print('Waiting...')
        #     time.sleep(1)
        # print(r.get())
        #
        # r = pool.starmap_async(n_times_fibo, [(100,)] * 100)
        # print(r)
        # while not r.ready():
        #     print('Waiting...')
        #     time.sleep(1)
        # print(r.get())
        #

        task_list = [pool.apply_async(n_times_fibo, (100,)) for _ in range(100)]
        while not all(t.ready() for t in task_list):
            print(f'{len([t for t in task_list if t.ready()])} / {len(task_list)}')
            time.sleep(1)
        print([t.get() for t in task_list] )
```
### `concurrent.futures.ProcessPoolExecutor()`
```python
import concurrent.futures
import multiprocessing

def fibo(n):
    x1 = 1
    x2 = 1
    for _ in range(n - 1):
        x1, x2 = x2, x1 + x2
    return x2


def n_times_fibo(m):
    for i in range(m):
        fibo(20_000)
    return f'{multiprocessing.current_process().name} result.'


if __name__ == '__main__':
    with concurrent.futures.ProcessPoolExecutor(16) as ex:
        task_list = [ex.submit(n_times_fibo, p) for p in [2000] + [100] * 100]
        for r in concurrent.futures.as_completed(task_list):
            print(r.result())
```