# Functions & Functional Programming

## Function and callables

### Callable instances

```python
import socket

class Resolver:
    def __init__(self):
        self._cache = {}

    def __call__(self, host):
        if host not in self._cache:
            self._cache[host] = socket.gethostbyname(host)
        return self._cache[host]
```
```python
>>> from resolver import Resolver
>>> resolve = Resolver()
>>> resolve('khorium.pl')
'116.203.144.128'
>>> resolve._cache
{'khorium.pl': '116.203.144.128'}
```

### Lambdas
```python
lambda args: expr
```

## Extended argument and call syntax

### Arbitraty positional arguments `*args`
```python
>>> def hypervolume(lenght, *lengths):
...     v = length                                                              
...     for item in lengths:                                                    
...         v *= item                                                           
...     return v                                                                
...                                                                             
>>> hypervolume(3, 5, 7, 9)                                                     
945                                                                             
>>> hypervolume(3, 5, 7)                                                        
105                                                                             
>>> hypervolume(3, 5)                                                           
15                                                                              
>>> hypervolume(3)                                                              
3   
```

### Arbitrary keywords arguments `**kwargs`
```python
>>> def tag(name, **attributes):                                                
...     result = '<' + name                                                     
...     for key, value in attributes.items():                                   
...         result += ' {k}="{v}"'.format(k=key, v=str(value))                  
...     result += '>'                                                           
...     return result                                                           
...                                                                             
>>> tag('img', src="Monet.jpg", alt="Sunrise by Claude Monet", border=1)        
'<img src="Monet.jpg" alt="Sunrise by Claude Monet" border="1">' 
```

### Extended call syntax
#### Unpacking
```python
>>> def print_args(arg1, arg2, *args):                                          
...     print(arg1)                                                             
...     print(arg2)                                                             
...     print(args)                                                             
...                                                                             
>>> t = (11, 12, 13, 14)                                                        
>>> print_args(*t)
11                                                                              
12                                                                              
(13, 14)       
```
```python
>>> def color(red, green, blue, **kwargs):                                      
...     print("r =", red)                                                       
...     print("g =", green)                                                     
...     print("b =", blue)                                                      
...     print(kwargs)                                                           
...                                                                             
>>> k = {'red':21, 'green':68, 'blue':120, 'alpha':52 }                         
>>> color(**k)                                                                  
r = 21                                                                          
g = 68                                                                          
b = 120                                                                         
{'alpha': 52}  
```
#### Argument forwarding
```python
>>> def trace(f, *args, **kwargs):                                              
...     print("args =", args)                                                   
...     print("kwargs =", kwargs)                                               
...     result = f(*args, **kwargs)                                             
...     print("result =", result)                                               
...     return result                                                           
...                                                                             
>>> trace(int, "ff", base=16)                                                   
args = ('ff',)                                                                  
kwargs = {'base': 16}                                                           
result = 255                                                                    
255
```

## Local functions / closures

### Local function
```python
>>> def sort_by_last_letter(strings):                                           
...     def last_letter(s):                                                     
...        return s[-1]                                                         
...     return sorted(strings, key=last_letter)                                 
...                                                                             
>>> sort_by_last_letter(['hello', 'from', 'a', 'local', 'function'])            
['a', 'local', 'from', 'function', 'hello']  
```

### Name resolution
```python
>>> g = 'global'                                                                
>>> def outer(p='param'):                                                       
...    l = 'local'                                                              
...    def inner():                                                             
...        print(g, p, l)                                                       
...    inner()                                                                  
...                                                                             
>>> outer()                                                                     
global param local   
```

### Closure
```python
>>> def raise_to(exp):                                                          
...     def raise_to_exp(x):                                                    
...         return pow(x, exp)                                                  
...     return raise_to_exp                                                     
...                                                                             
>>> square = raise_to(2)                                                        
>>> square.__closure__                                                          
(<cell at 0x10543de50: int object at 0x1051baab0>,)                             
>>> square(5)                                                                   
25                                                                              
>>> square(9)                                                                   
81
```

## Decorators

### Function
```python
>>> def escape_unicode(f):                                                      
...     def wrap(*args, **kwargs):                                              
...         x = f(*args, **kwargs)                                              
...         return ascii(x)                                                     
...     return wrap                                                             
...                                                                             
>>> def northern_city():                                                        
...     return 'Tromsø'                                                         
...                                                                             
>>> print(northern_city())                                                      
Tromsø                                                                          
>>> @escape_unicode                                                             
... def northern_city():                                                        
...     return 'Tromsø'                                                         
...                                                                             
>>> print(northern_city())                                                      
'Troms\xf8' 
```

### Class
```python
>>> class CallCount:                                                            
...     def __init__(self, f):                                                  
...         self.f = f                                                          
...         self.count = 0                                                      
...     def __call__(self, *args, **kwargs):                                    
...         self.count += 1                                                     
...         return self.f(*args, **kwargs)                                      
...                                                                             
>>> @CallCount                                                                  
... def hello(name):                                                            
...     print('Hello, {}!'.format(name))                                        
...                                                                             
>>> hello('Fred')                                                               
Hello, Fred!                                                                    
>>> hello('Wilma')                                                              
Hello, Wilma!                                                                   
>>> hello('Betty')                                                              
Hello, Betty!                                                                   
>>> hello('Barney')                                                             
Hello, Barney!                                                                  
>>> hello.count                                                                 
4 
```
```python
import functools

class MyDecorator:
    def __init__(self, f):
        self.f = f

    def __call__(self, *args, **kwargs):
        print('before function call', args, kwargs)
        r = self.f(*args, **kwargs)
        print('after function call', r)
        return r

    def __get__(self, instance, owner):
        return functools.partial(self, instance)

class A:
    @MyDecorator
    def f1(self, x, y):
        print('f1', x, y)
        return x + y
```

### Metadata
```python
>>> import functools                                                            
>>> def noop(f):                                                                
...     @functools.wraps(f)                                                     
...     def noop_wrapper():                                                     
...         return f()                                                          
...     return noop_wrapper                                                     
...                                                                             
>>> @noop                                                                       
... def hello():                                                                
...     "Print a well-known message."                                           
...     print('hello world!')                                                   
...                                                                             
>>> help(hello)                                                                 
Help on function hello in module __main__:                                      
                                                                                
hello()                                                                         
    Print a well-known message.                                                 
(END)                                                                           
                                                                                
>>> hello.__name__                                                              
'hello'                                                                         
>>> hello.__doc__                                                               
'Print a well-known message.' 
```

## Multi-input & nested comprehensions

### Single input comprehension
```python
>>> l = [i * 2 for i in range(10)]                                              
>>> d = {i: i * 2 for i in range(10)}                                           
>>> type(d)                                                                     
<class 'dict'>                                                                  
>>> s = {i for i in range(10)}                                                  
>>> type(s)                                                                     
<class 'set'>                                                                   
>>> g = (i for i in range(10))                                                  
>>> type(g)                                                                     
<class 'generator'>   
```
```python
vals = [expression for value in collection if condition]

# This is equivalent to:
vals = []
for value in collection:
    if condition:
        vals.append(expression)

# Example:
>>> even_squares = [x * x for x in range(10) if not x % 2]
>>> even_squares
[0, 4, 16, 36, 64]
```

### Multi-input comprehension
```python
>>> [(x, y) for x in range(5) for y in range(5)]                                
[(0, 0), (0, 1), (0, 2), (0, 3), (0, 4), (1, 0), (1, 1), (1, 2), (1, 3), (1, 4),
 (2, 0), (2, 1), (2, 2), (2, 3), (2, 4), (3, 0), (3, 1), (3, 2), (3, 3), (3, 4),
 (4, 0), (4, 1), (4, 2), (4, 3), (4, 4)]                                        
>>> points = []                                                                 
>>> for x in range(5):                                                          
...     for y in range(5):                                                      
...         points.append((x, y))                                               
...                                                                             
>>> 
```
```python
>>> values = [x / (x - y) for x in range(100) if x > 50 for y in range(100) if x
 - y != 0]                                                                      
>>> values = [x / (x - y)                                                       
...           for x in range(100)                                               
...           if x > 50                                                         
...           for y in range(100)                                               
...           if x - y != 0]                                                    
>>> values = []                                                                 
>>> for x in range(100):                                                        
...     if x > 50:                                                              
...         for y in range(100):                                                
...             if x - y != 0:                                                  
...                 values.append(x / (x - y))                                  
...
```
```py                                                                             
>>> [(x, y) for x in range(10) for y in range(x)]                               
[(1, 0), (2, 0), (2, 1), (3, 0), (3, 1), (3, 2), (4, 0), (4, 1), (4, 2), (4, 3),
 (5, 0), (5, 1), (5, 2), (5, 3), (5, 4), (6, 0), (6, 1), (6, 2), (6, 3), (6, 4),
 (6, 5), (7, 0), (7, 1), (7, 2), (7, 3), (7, 4), (7, 5), (7, 6), (8, 0), (8, 1),
 (8, 2), (8, 3), (8, 4), (8, 5), (8, 6), (8, 7), (9, 0), (9, 1), (9, 2), (9, 3),
 (9, 4), (9, 5), (9, 6), (9, 7), (9, 8)]                                        
>>> result = []                                                                 
>>> for x in range(10):                                                         
...     for y in range(x):                                                      
...         result.append((x, y))                                               
...    
```

### Nested comprehension
```python
>>> vals = [[y * 3 for y in range(x)] for x in range(10)]                       
>>> outer = []                                                                  
>>> for x in range(10):                                                         
...     inner = []                                                              
...     for y in range(x):                                                      
...         inner.append(y * 3)                                                 
...     outer.append(inner)                                                     
...                                                                             
>>> vals                                                                        
[[], [0], [0, 3], [0, 3, 6], [0, 3, 6, 9], [0, 3, 6, 9, 12], [0, 3, 6, 9, 12, 15
], [0, 3, 6, 9, 12, 15, 18], [0, 3, 6, 9, 12, 15, 18, 21], [0, 3, 6, 9, 12, 15, 
18, 21, 24]]    
```