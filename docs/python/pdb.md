# pdb
### Break into debugger
```python
import pdb; pdb.set_trace()
# or
breakpoint()
```
### From command-line
```python
python3 -m pdb main.py arg1 arg2
# or for module
python3 -m pdb -m main
```
# Essential commands
### Printing
```python
(Pdb) h p
p expression
        Print the value of the expression.
(Pdb) h pp
pp expression
        Pretty-print the value of the expression.
(Pdb) h l
l(ist) [first [,last] | .]
        List source code for the current file.  Without arguments,
        list 11 lines around the current line or continue the previous
        listing.  With . as argument, list 11 lines around the current
        line.  With one argument, list 11 lines starting at that line.
        With two arguments, list the given range; if the second
        argument is less than the first, it is a count.
(Pdb) h ll
longlist | ll
        List the whole source code for the current function or frame.
```
### Stepping through code
```Python
(Pdb) h n
n(ext)
        Continue execution until the next line in the current function
        is reached or it returns.
(Pdb) h s
s(tep)
        Execute the current line, stop at the first possible occasion
        (either in a function that is called or in the current
        function).
(Pdb) h b
b(reak) [ ([filename:]lineno | function) [, condition] ]
        Without argument, list all breaks.
        With a line number argument, set a break at this line in the
        current file.  With a function name, set a break at the first
        executable line of that function.  If a second argument is
        present, it is a string specifying an expression which must
        evaluate to true before the breakpoint is honored.
(Pdb) h c
c(ont(inue))
        Continue execution, only stop when a breakpoint is encountered.
(Pdb) h cl
cl(ear) filename:lineno
cl(ear) [bpnumber [bpnumber...]]
        With a space separated list of breakpoint numbers, clear
        those breakpoints.  Without argument, clear all breaks (but
        first ask confirmation).  With a filename:lineno argument,
        clear all breaks at that line in that file.
(Pdb) h unt
unt(il) [lineno]
        Without argument, continue execution until the line with a
        number greater than the current one is reached.  With a line
        number, continue execution until a line with a number greater
        or equal to that is reached.  In both cases, also stop when
        the current frame returns.
```
### Displaying expressions
```Python
(Pdb) h display
display [expression]
        Display the value of the expression if it changed, each time execution
        stops in the current frame.
        Without expression, list all display expressions for the current frame.
```
### Stack trace
```python
(Pdb) h w
w(here)
        Print a stack trace, with the most recent frame at the bottom.
        An arrow indicates the "current frame", which determines the
        context of most commands.  'bt' is an alias for this command.
(Pdb) h u
u(p) [count]
        Move the current frame count (default one) levels up in the
        stack trace (to an older frame).
(Pdb) h d
d(own) [count]
        Move the current frame count (default one) levels down in the
        stack trace (to a newer frame).
```