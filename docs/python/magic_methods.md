# Magic methods
```python
class A:
	def __bool__(self):
		return False

	def __enter__(self):
		...

	def __exit__(self):
		...
	
	def __add__(self):
		...
...
```
## Context manager
```python
from datetime import datetime

class Stopwatch:
    def __init__(self, msg) -> None:
        self.msg = msg

    def __enter__(self):
        self.start_time = datetime.now()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.end_time = datetime.now()
        self.duration = self.end_time - self.start_time
        print(f"{exc_type=}, {exc_val=}, {exc_tb=}")
        # return True <- block raising errors

    def __repr__(self) -> str:
        return f"{self.msg}: {self.duration.microseconds} ms"


if __name__ == "__main__":
    stopwatch = Stopwatch("100 000 inserts")
    with stoper as s:
        x = []
        for i in range(100_000):
            x.insert(0, 1)
    print(s)
    print(stoper)
```
## `__repr__, __eq__, __hash__`
```python
class A:
	__slots__ = ["atr1", "atr2"]
	
	def __init__(self, atr1, atr2):
	self.atr1 = atr1
	self.atr2 = atr2  

	def __repr__(self):
		return f'A(id={id(self)}, atr1={self.atr1}, atr2={self.atr2})'	 
	
	def __eq__(self, other):
		return self.atr1 == other.atr1 and self.atr2 == other.atr2	  
	
	def __hash__(self):
		return hash((self.atr1, self.atr2))
```

```python
MyClass.__mro__
```