# Useful tools and libs
### `map()`
```python
>>> result = map(print, "Ala ma kota")
>>> next(result)
A
>>> next(result)
l
>>> next(result)
a
>>> next(result)
 
>>> next(result)
m
>>> next(result)
a
>>> next(result)
```
```python
>>> result = map(print, "Ala ma kota", "Kota ma Ala")
>>> next(result)
A K
>>> next(result)
l o
>>> next(result)
a t
>>> next(result)
  a
>>> next(result)
m  
>>> 
```

### `filter()`
```python
>>> positives = filter(lambda x: x > 0, [1, -5, 0, 6, -2, 8])                   
>>> positives                                                                   
<filter object at 0x10f2be8b0>                                                  
>>> list(positives)                                                             
[1, 6, 8]    
```

### `functools.reduce()`
```python
>>> import operator     
>>> reduce(operator.add, [1, 2, 3, 4, 5])                                       
15  
```

### `defaultdict()`
```python
import collections

# empty dict of lists without
my_dict = collections.defaultdict(list)
```

### Map-reduce
```python
>>> def count_words(doc):                                                       
...     normalised_doc = ''.join(c.lower() if c.isalpha() else ' ' for c in doc)
...     frequencies = {}                                                        
...     for word in normalised_doc.split():                                     
...         frequencies[word] = frequencies.get(word, 0) + 1                    
...     return frequencies                                                      
...                                                                             
>>> count_words('It was the best of times, it was the worst of times.')         
{'it': 2, 'was': 2, 'the': 2, 'best': 1, 'of': 2, 'times': 2, 'worst': 1}       
>>> documents = [                                                               
...     'It was the best of times, it was the worst of times.',                 
...     'I went to the woods because I wished to live deliberately, to front onl
y the essential facts of life...',                                              
...     'Friends, Romans, countrymen, lend me your ears; I come to bury Caesar, 
not to praise him.',                                                            
...     'I do not like green eggs and ham. I do not like them, Sam-I-Am.',      
... ]                                                                           
>>> counts = map(count_words, documents)                                        
>>> def combine_counts(d1, d2):                                                 
...     d = d1.copy()                                                           
...     for word, count in d2.items():                                          
...         d[word] = d.get(word, 0) + count                                    
...     return d                                                                
...                                                                             
>>> from functools import reduce                                                
>>> total_counts = reduce(combine_counts, counts)                               
>>> total_counts                                                                
{'it': 2, 'was': 2, 'the': 4, 'best': 1, 'of': 3, 'times': 2, 'worst': 1, 'i': 6
, 'went': 1, 'to': 5, 'woods': 1, 'because': 1, 'wished': 1, 'live': 1, 'deliber
ately': 1, 'front': 1, 'only': 1, 'essential': 1, 'facts': 1, 'life': 1, 'friend
s': 1, 'romans': 1, 'countrymen': 1, 'lend': 1, 'me': 1, 'your': 1, 'ears': 1, '
come': 1, 'bury': 1, 'caesar': 1, 'not': 3, 'praise': 1, 'him': 1, 'do': 2, 'lik
e': 2, 'green': 1, 'eggs': 1, 'and': 1, 'ham': 1, 'them': 1, 'sam': 1, 'am': 1} 

```

