# PDM

### Installation
```bash
curl -sSLO https://pdm-project.org/install-pdm.py
curl -sSL https://pdm-project.org/install-pdm.py.sha256 | shasum -a 256 -c -
python3 install-pdm.py [options]
pdm completion bash |sudo tee /etc/bash_completion.d/pdm
```
### Central location for virtualenvs
```
pdm config venv.in_project false
pdm config venv.location ~/.virtualenvs/
```
### Project management
```bash
# create new project
pdm init 
# show information about project
pdm info
# change python version
pdm use
pdm use -f /path/to/venv
pdm use [--venv <name>] 
# import metadata
pdm import
```
### Dependencies
```bash
# add/remove deps
pdm add/remove <pkg>
# add dev deps
pdm add -d flake8
pdm add -dG test pytest
# update deps
pdm update [<pkg>]
pdm sync
pdm install
# export requirements
pdm export -o requirements.txt
```
### Configuration
```bash
# show config [variable]
pdm config
# set value 
pdm config <variable> <value> [--local]
```
### Virtualenvs
```bash
# create virtualenv
pdm venv create [--name <name>] [<python_version>]
# list venvs
pdm venv list
# remove venv
pdm venv remove <name>
# run command in venv 
pdm run [--venv <name>] <command>
# installed pkgs in venv
pdm list [--venv <name>]
```
### Bash function for shell activation
```bash
pdm() {
  local command=$1

  if [[ "$command" == "shell" ]]; then
      eval $(pdm venv activate)
  else
      command pdm $@
  fi
}
```