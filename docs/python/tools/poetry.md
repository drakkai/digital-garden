# Poetry

### Installation
```
curl -sSL https://install.python-poetry.org | python3 -
```
### Autocomplete
```
poetry completions bash > /etc/bash_completion.d/poetry
```
### Basic command
```shell
# Setup basic project
poetry new [--src | --name] <package_name>
# or
poetry init [--no-interaction]

# Create venv and nstall deps
poetry install 

# Run command in virtualenv
poetry run <command>

# Environment commands
poetry env list/info/remove

# Add package
poetry add [--dev] <package_name>
```

### Configuration
```shell
poetry config <key> <value>
poetry config --list/--unset
```
 config file: `~/.config/pypoetry/config.toml`
### Convert requirements.txt to pyproject.toml
1. Install `dephell`
```
pip install dephell
```
2. Create `pyproject.toml` file with following section
```toml
[tool.dephell.main]
from = {format = "pip", path = "requirements.txt"}
to = {format = "poetry", path = "pyproject.toml"}
```
_one file for dephell settings and poetry dependencies_

3. Convert type
```
dephell deps convert
```
_dephell read source and output file from pyproject.toml_

4. Add poetry settings in `[tool.poetry]`:
      * name
      * version
      * description
      * authors

Example input file (requirements.txt):
```txt
docutils==0.16
Markdown==3.1.1
parso==0.6.0
pytest==5.3.5
pytest-json-report==1.2.1
pytest-metadata==1.8.0
pytest-sugar==0.9.2
PyYAML==5.3
redbaron==0.9.2
typer==0.0.8
```
Output `pyproject.toml`:
```toml
[tool.poetry]
name = "ssg"
version = "0.0.0"
description = ""
authors = []

[tool.poetry.dependencies]
python = ">=3.6"
docutils = "==0.16"
markdown = "==3.1.1"
parso = "==0.6.0"
pytest = "==5.3.5"
pytest-json-report = "==1.2.1"
pytest-metadata = "==1.8.0"
pytest-sugar = "==0.9.2"
pyyaml = "==5.3"
redbaron = "==0.9.2"
typer = "==0.0.8"


[build-system]
requires = ["poetry-core>=1.0.0"]
build-backend = "poetry.core.masonry.api"

[tool.dephell.main]
from = {format = "pip", path = "requirements.txt"}
to = {format = "poetry", path = "pyproject.toml"}
```