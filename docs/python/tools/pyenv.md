# pyenv
## Installation
```sh
curl https://pyenv.run | bash
```

Install dependencies (Ubuntu):
```sh
sudo apt update && sudo apt install make build-essential \
wget curl git \
libbz2-dev libreadline-dev libffi-dev python-openssl \
libssl-dev libsqlite3-dev liblzma-dev
```

## Setup
Add to `~/.bashrc`
```sh
export PATH="~/.pyenv/bin:$PATH"
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"
```

## Install new version
```sh
pyenv install 3.10.8
```

In project dir:
```sh
pyenv local 3.10.8
```

