# Virtualenv / virtualenvwrapper 

### Installation
```
pip install virtualenv --user
pip install virtualenvwrapper --user
```
## Virtualenv
Create virtual env in provided path
```
virtualenv ~/.virtualenvs/testenv
```
Default config location
```
.config/virtualenv/virtualenv.ini
```

## Virtualenwrapper
### Create dir for virtual envs and initialize
(add to `.bashrc` for example)
```sh
export WORKON_HOME="~/.virtualenvs"
export PROJECT_HOME="~/code/python"
export VIRTUALENVWRAPPER_SCRIPT=/home/drakkai/.local/bin/virtualenvwrapper.sh
source /home/drakkai/.local/bin/virtualenvwrapper_lazy.sh
```
```sh
mkdir ~/.virtualenvs
source ~/.bashrc
```
### Basic commands
```sh
mkvirtualenv testenv #create env
workon testenv #activate env
workon #list of envs
mkproject testenv #create project (venv, dir etc.)
deactivate 
cdproject 
cdvirtualenv
setvirtualenvproject #bind env to project
```