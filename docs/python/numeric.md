# Numeric types, dates and times

### Decimal module
```python
>>> from decimal import Decimal                                                 
>>> Decimal(7)                                                                  
Decimal('7')                                                                    
>>> Decimal('0.8')                                                              
Decimal('0.8')                                                                  
>>> Decimal('0.8') - Decimal('0.7')                                             
Decimal('0.1')
>>> decimal.getcontext().prec = 6                                               
>>> d = Decimal('1.234567')                                                     
>>> d                                                                           
Decimal('1.234567')                                                             
>>> d + Decimal(1)                                                              
Decimal('2.23457') 
```

### Fractions module
```python
>>> from fractions import Fraction                                              
>>> two_thirds = Fraction(2, 3)                                                 
>>> two_thirds                                                                  
Fraction(2, 3)                                                                  
>>> four_fifths = Fraction(4, 5)                                                
>>> four_fifths                                                                 
Fraction(4, 5)    
>>> Fraction(0.5) 
Fraction(1, 2)                                                                  
>>> Fraction(0.1)                                                               
Fraction(3602879701896397, 36028797018963968)                                   
>>> Fraction(Decimal('0.1'))                                                    
Fraction(1, 10)                                                                 
>>> Fraction('22/7')                                                            
Fraction(22, 7)                                                                 
>>> Fraction(2, 3) + Fraction(4, 5)                                             
Fraction(22, 15)                                                                
>>> Fraction(2, 3) - Fraction(4, 5)                                             
Fraction(-2, 15)                                                                
>>> Fraction(2, 3) * Fraction(4, 5)                                             
Fraction(8, 15)                                                                 
>>> Fraction(2, 3) / Fraction(4, 5)                                             
Fraction(5, 6)                                                                  
>>> Fraction(2, 3) // Fraction(4, 5)                                            
0                                                                               
>>> Fraction(2, 3) % Fraction(4, 5)                                             
Fraction(2, 3)       
```

### Complex numbers
```python
>>> 2j                                                                          
2j                                                                              
>>> type(2j)                                                                    
<class 'complex'>                                                               
>>> 3 + 4j                                                                      
(3+4j)                                                                          
>>> type(3 + 4j)                                                                
<class 'complex'>                                                               
>>> complex(3)                                                                  
(3+0j)                                                                          
>>> complex(-2, 3)                                                              
(-2+3j)                                                                         
>>> complex('(-2+3j)')                                                          
(-2+3j)                                                                         
>>> complex('-2+3j')                                                            
(-2+3j)  
```
```python
>>> def inductive(ohms):
...     return complex(0.0, ohms)                                               
...                                                                             
>>> def capacitive(ohms):                                                       
...     return complex(0.0, -ohms)                                              
...                                                                             
>>> def resistive(ohms):                                                        
...     return complex(ohms)                                                    
...                                                                             
>>> def impedance(components):                                                  
...     z = sum(components)                                                     
...     return z                                                                
...                                                                             
>>> impedance([inductive(10), resistive(10), capacitive(5)])                    
(10+5j)                                                                         
>>> import cmath                                                                
>>> cmath.phase(_)                                                              
0.4636476090008061                                                              
>>> import math                                                                 
>>> math.degrees(_)                                                             
26.56505117707799 
```

### Built-in functions
```python
>>> abs(-5)
5                                                                               
>>> abs(-5.0)                                                                   
5.0                                                                             
>>> abs(Decimal(-5))                                                            
Decimal('5')                                                                    
>>> abs(Fraction(-5, 1))                                                        
Fraction(5, 1)                                                                  
>>> abs(complex(0, -5))                                                         
5.0                                                                             
>>> round(0.2812, 3)                                                            
0.281                                                                           
>>> round(0.625, 1)                                                             
0.6                                                                             
>>> round(1.5)                                                                  
2                                                                               
>>> round(2.5)                                                                  
2                                                                               
>>> round(867)                                                                  
867                                                                             
>>> round(53.09)                                                                
53                                                                              
>>> round(Decimal('3.25'), 1)                                                   
Decimal('3.2')                                                                  
>>> round(Fraction(57, 100), 2)                                                 
Fraction(57, 100) 
```
```python
>>> 0b101010
42                                                                              
>>> 0o52                                                                        
42                                                                              
>>> 0x2a                                                                        
42                                                                              
>>> bin(42)                                                                     
'0b101010'                                                                      
>>> oct(42)                                                                     
'0o52'                                                                          
>>> hex(42)                                                                     
'0x2a'                                                                          
>>> hex(42)[2:]                                                                 
'2a'                                                                            
>>> int("2a", base=16)                                                          
42  
```

### Dates and times - datetime module
#### Date
```python
>>> import datetime
>>> datetime.date.today()
datetime.date(2021, 5, 3)
>>> d = datetime.date.today() 
>>> d.year
d.year
>>> d.year
2021
>>> d.month
5
>>> d.day
3
>>> d.isoformat()
'2021-05-03'
>>> "The date is {:%A %d %B %Y}".format(d) 
'The date is Monday 03 May 2021'
>>> d.strftime('%A %d %B %Y')
'Monday 03 May 2021'
```
#### Time
```python
>>> t = datetime.time(10, 32, 47, 675623)
>>> t.hour
10
>>> t.minute
32
>>> t.second
47
>>> t.microsecond
675623
>>> t.isoformat()
'10:32:47.675623'
>>> t.strftime('%Hh%Mm%Ss')
'10h32m47s'
>>> "{t.hour}h{t.minute}m{t.second}s".format(t=t)
'10h32m47s'
```
#### Date and time
```python
>>> import datetime
>>> datetime.datetime.today()
datetime.datetime(2021, 5, 3, 14, 40, 0, 169177)
>>> datetime.datetime.now()
datetime.datetime(2021, 5, 3, 14, 40, 4, 763035)
>>> datetime.datetime.utcnow()
datetime.datetime(2021, 5, 3, 12, 40, 12, 309800)
>>> d = datetime.date.today()
>>> t = datetime.time(8, 15)
>>> datetime.datetime.combine(d, t)
datetime.datetime(2021, 5, 3, 8, 15)
>>> dt = datetime.datetime.strptime("Monday 6 January 2014, 12:13:31", "%A %d %B %Y, %H:%M:%S")
>>> dt.date()
datetime.date(2014, 1, 6)
>>> dt.time()
datetime.time(12, 13, 31)
>>> dt.day
6
>>> dt.isoformat()
'2014-01-06T12:13:31'
>>>
```
#### Durations
```python
>>> td = datetime.timedelta(weeks=1, minutes=2, milliseconds=5500)
>>> td
datetime.timedelta(days=7, seconds=125, microseconds=500000)
>>> td.days
7
>>> td.seconds
125
>>> td.microseconds
500000
>>> str(td)
'7 days, 0:02:05.500000'
>>> repr(td)
'datetime.timedelta(days=7, seconds=125, microseconds=500000)'
>>> a = datetime.datetime(year=2014, month=5, day=8, hour=14, minute=22)
>>> a
datetime.datetime(2014, 5, 8, 14, 22)
>>> b = datetime.datetime(year=2014, month=3, day=14, hour=12, minute=9)
>>> b
datetime.datetime(2014, 3, 14, 12, 9)
>>> d = a - b
>>> d
datetime.timedelta(days=55, seconds=7980)
>>> d.total_seconds()
4759980.0
>>> datetime.date.today() + datetime.timedelta(weeks=1) * 3
datetime.date(2021, 5, 24)
```
#### Timezones
```python
>>> cet = datetime.timezone(datetime.timedelta(hours=1), "CET")
>>> cet
datetime.timezone(datetime.timedelta(seconds=3600), 'CET')
>>> departure = datetime.datetime(year=2014, month=1, day=7, hour=11, minute=30, tzinfo=cet)
>>> arrival = datetime.datetime(year=2014, month=1, day=7, hour=13, minute=5, tzinfo=datetime.timezone.utc)
>>> arrival - departure
datetime.timedelta(seconds=9300)
>>> str(arrival - departure)
'2:35:00'  
```