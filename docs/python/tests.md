# Tests
## Pytest

```shell
# disable warnings
pytest -p no:warnings

# run only the last failed tests
pytest --lf

# run only with names that match the string expression
pytest -k "get and not test_get_response"

# stop after the first failure [and enter pdb]
pytest -x [--pdb]

# stop after two failures
pytest --maxfail=2

# show local variables in tracebacks
pytest -l

# list the 2 slowest tests
pytest --durations=2
```

### pyproject.toml

```toml
[tool.pytest.ini_options]
addopts = "-p no:warnings -v"
```

### Useful libs
- https://pypi.org/project/pytest-sugar/
- https://pypi.org/project/pytest-clarity/
