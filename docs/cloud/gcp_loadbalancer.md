# [GCP] http loadbalancer
### Install nginx 
```console
cat << EOF > startup.sh
#! /bin/bash
apt-get update
apt-get install -y nginx
service nginx start
sed -i — ‘s/nginx/Google Cloud Platform — ‘“\$HOSTNAME”’/’ /var/www/html/index.nginx-debian.html
EOF
```
### Set up region and zone
```console
gcloud config set compute/region us-east1
gcloud config set compute/zone us-east1-b
```

## Steps
1. Create an instance template.

`gcloud compute instance-templates create nginx-template --machine-type=f1-micro --metadata-from-file startup-script=startup.sh`

2. Create a target pool.

`gcloud compute target-pools create nginx-pool`

3. Create a managed instance group.

`gcloud compute instance-groups managed create nginx-group --base-instance-name nginx --size 2 --template nginx-template --target-pool nginx-pool`

4. Create a firewall rule to allow traffic (80/tcp).

`gcloud compute firewall-rules create www-firewall --allow tcp:80`

5. Create a health check.

`gcloud compute health-checks create http http-basic-check --port 80`

`gcloud compute instance-groups managed set-named-ports nginx-group --named-ports http:80`

6. Create a backend service, and attach the managed instance group.

`gcloud compute backend-services create nginx-backend --protocol HTTP --port-name http --health-checks http-basic-check --global`

`gcloud compute backend-services add-backend nginx-backend --instance-group nginx-group --instance-group-zone us-east1-b --global`

7. Create a URL map, and target the HTTP proxy to route requests to your URL map.

`gcloud compute url-maps create web-map --default-service nginx-backend`

`gcloud compute target-http-proxies create http-lb-proxy --url-map web-map`

8. Create a forwarding rule.

`gcloud compute forwarding-rules create http-content-rule --global --target-http-proxy http-lb-proxy --ports 80`
