# First app

The best options to check if everything went well with installation is to deploy simply application like web server and perform some basic operations. 

## Deploy

Deploy app (Apache in this case), check if pod is running, list and describe deployment. Replicaset is also created. Delete pod to check the controller manager (new pod should be created)

```console
k8s-master:~$ kubectl create deployment website --image=httpd
deployment.apps/website created

k8s-master:~$ kubectl get pods
NAME                       READY   STATUS    RESTARTS   AGE
website-54b59c94b8-gmgmd   1/1     Running   0          114s

k8s-master:~$ kubectl get deployments
NAME      READY   UP-TO-DATE   AVAILABLE   AGE
website   1/1     1            1           3m6s

k8s-master:~$ kubectl describe deployments website
Name:                   website
Namespace:              default
CreationTimestamp:      Mon, 04 May 2020 17:34:37 +0000
Labels:                 app=website
Annotations:            deployment.kubernetes.io/revision: 1
Selector:               app=website
Replicas:               1 desired | 1 updated | 1 total | 1 available | 0 unavailable
StrategyType:           RollingUpdate
MinReadySeconds:        0
RollingUpdateStrategy:  25% max unavailable, 25% max surge
Pod Template:
  Labels:  app=website

k8s-master:~$ kubectl get replicasets.apps
NAME                 DESIRED   CURRENT   READY   AGE
website-54b59c94b8   1         1         1       21h

k8s-master:~$ kubectl delete pod website-54b59c94b8-gmgmd
pod "website-54b59c94b8-gmgmd" deleted

k8s-master:~$ kubectl get pods
NAME                       READY   STATUS    RESTARTS   AGE
website-54b59c94b8-bffx8   1/1     Running   0          5m27s
```

## Remove and deploy new

Delete the previously created deployment 

```console
k8s-master:~$ kubectl delete deployments website
deployment.apps "website" deleted

k8s-master:~$ kubectl get pods
NAME                       READY   STATUS        RESTARTS   AGE
website-54b59c94b8-bffx8   0/1     Terminating   0          9m35s

```

Create new one from yaml file. The new one will be nginx web server with 2 replicas and exposed port 80

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
  labels:
    app: nginx
spec:
  replicas: 2
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:1.18
        ports:
        - containerPort: 80
```

* `.metadata.name` - name of deployment
* `.spec.replicas` - numer of pod manage by the deployment
* `.spec.selector` - defines which pods manage the deployment 
* `.spec.template` - describe the pods: labels (matched to selector), containers, image, exposed ports

```console
k8s-master:~$ kubectl create -f nginx.yaml
deployment.apps/nginx-deployment created

k8s-master:~$ kubectl get pods,deployments
NAME                                    READY   STATUS              RESTARTS   AGE
pod/nginx-deployment-5d85b5fb59-8z7xl   1/1     Running             0          21s
pod/nginx-deployment-5d85b5fb59-dzgpx   0/1     ContainerCreating   0          21s

NAME                               READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/nginx-deployment   1/2     2            1           21s

k8s-master:~$ kubectl get replicasets
NAME                          DESIRED   CURRENT   READY   AGE
nginx-deployment-5d85b5fb59   2         2         2       50s
```

## Scale the deployment

Changes can be made to deployment interactively by `kubectl edit` or applied from file by `kubectl apply` command (declarative way). Example how to change number of replicas (this parameter can be also change by `kubectl scale` command):

```console
k8s-master:~$ sed -i 's/replicas: 2/replicas: 3/' nginx.yaml

k8s-master:~$ kubectl apply -f nginx.yaml
deployment.apps/nginx-deployment configured

k8s-master:~$ kubectl get pods,deployments
NAME                                    READY   STATUS    RESTARTS   AGE
pod/nginx-deployment-5d85b5fb59-8z7xl   1/1     Running   0          14m
pod/nginx-deployment-5d85b5fb59-dzgpx   1/1     Running   0          14m
pod/nginx-deployment-5d85b5fb59-mxnqx   1/1     Running   0          14s

NAME                               READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/nginx-deployment   3/3     3            3           14m
```

Scale down the deployment

```console
k8s-master:~$ kubectl scale deployment nginx-deployment --replicas=1
deployment.apps/nginx-deployment scaled

k8s-master:~$ kubectl get pods
NAME                                READY   STATUS        RESTARTS   AGE
nginx-deployment-5d85b5fb59-8z7xl   1/1     Terminating   0          39m
nginx-deployment-5d85b5fb59-dzgpx   1/1     Running       0          39m
nginx-deployment-5d85b5fb59-mxnqx   0/1     Terminating   0          25m
```

## Expose the port

New service will be created with cluster IP while expose the deployment. Endpoint IPs are IPs of the pods 

```console
k8s-master:~$ kubectl expose deployment nginx-deployment
service/nginx-deployment exposed

k8s-master:~$ kubectl get svc nginx-deployment
NAME               TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)   AGE
nginx-deployment   ClusterIP   10.104.97.108   <none>        80/TCP    11s

k8s-master:~$ kubectl get endpoints nginx-deployment
NAME               ENDPOINTS                                               AGE
nginx-deployment   192.168.130.68:80,192.168.89.131:80,192.168.89.132:80   32s
```

Request to the cluster IP or endpoint IP should have the same result

```console
k8s-master:~$ curl 10.104.97.108:80
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
[...]

k8s-master:~$ curl 192.168.89.131:80
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
[...]
```

From within container request can be also made using service name (node itself can't resolve it)

```console
k8s-master:~$ kubectl get pods
NAME                                READY   STATUS    RESTARTS   AGE
nginx-deployment-5d85b5fb59-8z7xl   1/1     Running   0          27m
nginx-deployment-5d85b5fb59-dzgpx   1/1     Running   0          27m
nginx-deployment-5d85b5fb59-mxnqx   1/1     Running   0          13m

k8s-master:~$ kubectl exec -it nginx-deployment-5d85b5fb59-8z7xl -- bash
root@nginx-deployment-5d85b5fb59-8z7xl:/# curl nginx-deployment
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
[...]
```

To expose the service outside the cluster, it needs to be exposed as a different type (not ClusterIP), like NodePort or LoadBlanacer (LB is actually not needed). In this example the website will be available on all nodes IPs (included master) on port 32394:

```console
k8s-master:~$ kubectl delete svc nginx-deployment
service "nginx-deployment" deleted

k8s-master:~$ kubectl expose deployment nginx-deployment --type=NodePort
service/nginx-deployment exposed

k8s-master:~$ kubectl get svc
NAME               TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)        AGE
kubernetes         ClusterIP   10.96.0.1      <none>        443/TCP        10d
nginx-deployment   NodePort    10.107.181.3   <none>        80:30338/TCP   3s

root@k8s-node1:~# netstat -plantu |grep 30338
tcp        0      0 0.0.0.0:30338           0.0.0.0:*               LISTEN      12014/kube-proxy

k8s-master:~$ kubectl expose deployment nginx-deployment --type=LoadBalancer
service/nginx-deployment exposed

k8s-master:~$ kubectl get svc
NAME               TYPE           CLUSTER-IP     EXTERNAL-IP   PORT(S)        AGE
kubernetes         ClusterIP      10.96.0.1      <none>        443/TCP        10d
nginx-deployment   LoadBalancer   10.106.20.70   <pending>     80:32394/TCP   2s

root@k8s-node1:~# netstat -plantu |grep 32394
tcp        0      0 0.0.0.0:32394           0.0.0.0:*               LISTEN      12014/kube-proxy
```

Service definition can be also added to yaml specification as it is a valid REST object

```yaml
apiVersion: v1
kind: Service
metadata:
  name: nginx-deployment
spec:
  type: NodePort
  selector:
    app: nginx
  ports:
    - nodePort: 30338
      protocol: TCP
      port: 80
      targetPort: 80
```

Service types are described [here](https://kubernetes.io/docs/concepts/services-networking/service/#publishing-services-service-types)

## Delete application
Keeping all application resources in yaml sepcification allow to remove them in easly way if something went wrong

```console
k8s-master:~$ kubectl get pods,deployment,svc
NAME                                    READY   STATUS    RESTARTS   AGE
pod/nginx-deployment-5d85b5fb59-lkh2x   1/1     Running   0          84s
pod/nginx-deployment-5d85b5fb59-tfzfp   1/1     Running   0          84s
pod/nginx-deployment-5d85b5fb59-wzvxh   1/1     Running   0          84s

NAME                               READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/nginx-deployment   3/3     3            3           85s

NAME                       TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)        AGE
service/kubernetes         ClusterIP   10.96.0.1        <none>        443/TCP        10d
service/nginx-deployment   NodePort    10.111.117.166   <none>        80:30338/TCP   83s

k8s-master:~$ kubectl delete -f nginx.yaml
deployment.apps "nginx-deployment" deleted
service "nginx-deployment" deleted

k8s-master:~$ kubectl get pods,deployment,svc
NAME                                    READY   STATUS        RESTARTS   AGE
pod/nginx-deployment-5d85b5fb59-lkh2x   1/1     Terminating   0          100s
pod/nginx-deployment-5d85b5fb59-tfzfp   1/1     Terminating   0          100s
pod/nginx-deployment-5d85b5fb59-wzvxh   1/1     Terminating   0          100s

NAME                 TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)   AGE
service/kubernetes   ClusterIP   10.96.0.1    <none>        443/TCP   10d
```