# Useful tools
## Arkade
### Installation
```
curl -sLS https://dl.get-arkade.dev | sh
sudo cp arkade /usr/local/bin/arkade
arkade completion bash |sudo tee /etc/bash_completion.d/arkade
```
### Available apps
```
arkade install --help
```
```
arkade install cert-manager
```

## k3d
### Installation
##linux
```
wget -q -O - https://raw.githubusercontent.com/rancher/k3d/main/install.sh | bash
```
windows
```
choco install k3d 
```
### First cluster
```
k3d cluster create test-cluster --servers 
```