# Kubernetes
---
## Tips & tricks
### Proxy server
```shell
kubectl proxy &
```
```console
$ k proxy --api-prefix=/ &
[1] 70864
Starting to serve on 127.0.0.1:8001
$ curl -s http://127.0.0.1:8001/apis |head
{
  "kind": "APIGroupList",
  "apiVersion": "v1",
  "groups": [
    {
      "name": "apiregistration.k8s.io",
      "versions": [
        {
          "groupVersion": "apiregistration.k8s.io/v1",
          "version": "v1"
[...]

$ curl -s http://127.0.0.1:8001/api/v1/namespaces |head
{
  "kind": "NamespaceList",
  "apiVersion": "v1",
  "metadata": {
    "selfLink": "/api/v1/namespaces",
    "resourceVersion": "18117"
  },
  "items": [
    {
      "metadata": {
[...]
```
### Port forward
```shell
export POD_NAME=$(kubectl get pods --namespace default -l "app.kubernetes.io/component=jenkins-master" -l "app.kubernetes.io/instance=cd" -o jsonpath="{.items[0].metadata.name}")

kubectl port-forward $POD_NAME 8080:8080 >> /dev/null &
```