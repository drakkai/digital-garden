# MicroK8s
<sup>Windows 10 setup</sup>

## Installation
1. Microk8s on windows uses Hyper-V
2. Download [installer](https://microk8s.io/microk8s-installer.exe) and run it
3. Status of MicroK8s can be checked with following command:
```
microk8s status --wait-ready
```
4. After MicroK8s is up, additional services can be enabled as follow:
```
microk8s enable dashboard dns
```
5. Check cluster
```console
$ microk8s.exe kubectl cluster-info
Kubernetes control plane is running at https://172.22.77.118:16443
CoreDNS is running at https://172.22.77.118:16443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy
```

## Configuration

MicroK8s can be managed with default `kubectl` binary instead of `microk8s kubectl`.

1. Generate config
```console
$ microk8s.exe config
apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: DATA+OMITTED
    server: https://172.22.77.118:16443
  name: microk8s-cluster
contexts:
- context:
    cluster: microk8s-cluster
    user: admin
  name: microk8s
current-context: microk8s
kind: Config
preferences: {}
users:
- name: admin
  user:
    token: DATA+OMITTED
```
2. Put it to `~/.kube/config` file (on Windows host or in WSL)
```console
$ vim ~/.kube/config
$ kubectl config use-context microk8s
Switched to context "microk8s".
$ kubectl cluster-info
Kubernetes control plane is running at https://172.22.77.118:16443
CoreDNS is running at https://172.22.77.118:16443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy
$ kubectl version
Client Version: version.Info{Major:"1", Minor:"20", GitVersion:"v1.20.2", GitCommit:"faecb196815e248d3ecfb03c680a4507229c2a56", GitTreeState:"clean", BuildDate:"2021-01-13T13:28:09Z", GoVersion:"go1.15.5", Compiler:"gc", Platform:"linux/amd64"}
Server Version: version.Info{Major:"1", Minor:"20+", GitVersion:"v1.20.1-34+e7db93d188d0d1", GitCommit:"e7db93d188d0d12f2fe5336d1b85cdb94cb909d3", GitTreeState:"clean", BuildDate:"2021-01-11T23:50:46Z", GoVersion:"go1.15.6", Compiler:"gc", Platform:"linux/amd64"}
```
