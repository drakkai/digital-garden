# Installation

### Installation tools

* **kubeadm** (community suggested)
* kubespray
* for aws: kops, kube-aws
* hyperkube (all-in-one container)

### Pod networking

* Calico (L3 network) - allows network policies
* Flannel (L4 network)
* Canal
* Weave Net
* Kube-router

## Installation steps

***Make sure that VMs have unique MAC and product_uuid***

### Master node
<sup>(based on ubuntu 18.04)</sup>

1. Install runtime: **docker** (used by default by kubeadm) or **cri-o**, which needs additional steps:
    * `modprobe overlay`
    * `modprobe br_netfilter`
    * make them persistent in `/etc/modules-load.d/99-kubernetes-cri.conf`
    * enable kernel parameters to allow iptables see bridged traffic:
    ```ini
    net.bridge.bridge-nf-call-iptables  = 1
    net.ipv4.ip_forward                 = 1
    net.bridge.bridge-nf-call-ip6tables = 1
    ```
    `sysctl --system`
    * install `software-properties-common`
    * `add-apt-repository ppa:projectatomic/ppa`
    * install `cri-o-1.15`
    * change in `/etc/crio/crio.conf` path to conmon `/usr/bin/conmon` (optional: add other registries `"registry.fedoraproject.org"` to `registries` in the same file)
    * reload systemd **daemons**, start and enable `crio` **service**
    * add **extra args** for kubelet `/etc/default/kubelet` - this will allow kubelet know how to interact with cri-o ([more](https://github.com/cri-o/cri-o/blob/master/tutorials/kubeadm.md))
    ```ini
    KUBELET_EXTRA_ARGS=\
    --feature-gates="AllAlpha=false,RunAsGroup=true" \
    --container-runtime=remote \
    --cgroup-driver=systemd \
    --container-runtime-endpoint='unix:///var/run/crio/crio.sock' \
    --runtime-request-timeout=5m
    ```
2. Add kubernetes repository and install `kubelet`, `kubeadm` and `kubectl`, also hold the version of them ([more](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/#installing-kubeadm-kubelet-and-kubectl))
```console
$ sudo apt-get update && sudo apt-get install -y apt-transport-https curl
$ curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
$ cat <<EOF | sudo tee /etc/apt/sources.list.d/kubernetes.list
deb https://apt.kubernetes.io/ kubernetes-xenial main
EOF
$ sudo apt-get update
$ sudo apt-get install -y kubelet kubeadm kubectl
$ sudo apt-mark hold kubelet kubeadm kubectl
```
3. Install pod network for **CNI** i.e. flannel 
    * download flannel yaml manifest
    ```
    wget https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
    ```
    * set IPv4 pool in `net-conf.json` section for Pods IPs - default is `10.244.0.0/16` (should be different than node IPs range)
4. Add a local DNS alias for master node in `/etc/hosts`
```console
$ cat /etc/hosts
127.0.0.1 localhost
172.16.0.10 k8s-master
172.16.0.11 k8s-node1
172.16.0.12 k8s-node2
```
5. Create a configuration yaml for the cluster, simple example for **control-plane node** ([more](https://kubernetes.io/docs/reference/setup-tools/kubeadm/kubeadm-init/#config-file))
```yaml
apiVersion: kubeadm.k8s.io/v1beta2
kind: ClusterConfiguration
kubernetesVersion: 1.19.4
controlPlaneEndpoint: "k8s-master:6443"
networking:
  podSubnet: 10.244.0.0/16
```
*podSubnet -> IP range from flannel*

6. Connectivity to registry can be check by
```
kubeadm config images pull
```
7. Initialize the master 
```
kubeadm init --config=kubeadm-config.yaml --upload-certs | tee kubeadm-init.out
```
8. Perpare configuration for regular user, as regular user type:
```
mkdir -p $HOME/.kube \
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config \
sudo chown $(id -u):$(id -g) $HOME/.kube/config 
```
1. Install Pod network addon 
```
kubectl apply -f kube-flannel.yaml
```
10. All pods (including calico and coredns) should be up and running

### Worker node

1. Install **docker** or **cri-o** for container runtime as described for master node
2. Add kubernetes repository and install kubeadm, kubelet and kubectl (optional), it's good to install the same version
3. On master node generate join token:
```
sudo kubeadm token create
```
4. Also on master node crate a Discovery Token CA Cert Hash
```
openssl x509 -pubkey \
-in /etc/kubernetes/pki/ca.crt | openssl rsa \
-pubin -outform der 2>/dev/null | openssl dgst \
-sha256 -hex | sed 's/ˆ.* //'
```
5. Update `/etc/hosts` for local DNS on all nodes
6. Join the worker node 
```
kubeadm join --token <master_token> <master_hostname>:6443 --discovery-token-ca-cert-hash sha256:<cert_hash>
```
1. It take few moments to download images, start and setup kube-proxy, calico-node
2. After all worker node should be in state `Ready` and pods running

### Optional

* Master node has a `NoSchedule` taint for security/performance reason
```console
$ kubectl describe node k8s-master |grep Taints
Taints:             node-role.kubernetes.io/master:NoSchedule
```
To allow non-system pods scheduling on master, run:
```
kubectl taint node k8s-master node.kubernetes.io/master-
```
* `coredns` pods may stucks during deploy process, simply remove them to fix 
```
kubectl delete pod coredns-
```