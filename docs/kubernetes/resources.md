# Resource limitations

## Limits per depolyment

To generate load we will use container with `stress` i.e. polinux/stress


```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: stress
  labels:
	app: stress
spec:
  replicas: 1
  selector:
	matchLabels:
	  app: stress
  template:
	metadata:
	  labels:
		app: stress
	spec:
	  containers:
	  - name: stress-alpine
		image: polinux/stress:1.0.4
		command: ["stress"]
		args: ["--cpu", "1", "--vm-bytes", "128M", "--verbose"]
		resources: {}
```

```console
k8s-master:~$ kubectl get deployments.apps
NAME     READY   UP-TO-DATE   AVAILABLE   AGE
stress   1/1     1            1           13m
k8s-master:~$ kubectl get pods
NAME                     READY   STATUS    RESTARTS   AGE
stress-c77d85776-prvvr   1/1     Running   0          87s
```

For now there are no limits for this pod, `resources` field is empty. Let's add limits for cpu and memory

```yaml
      containers:
      - name: stress-alpine
        image: polinux/stress:1.0.4
        command: ["stress"]
        args: ["--cpu", "3", "--vm-bytes", "4G", "--verbose"]
        resources:
          limits:
            memory: "256M"
            cpu: "1"
          requests:
            cpu: "0.5"
            memory: "128M"
```
After applying above changes, kubernetes will not allow `stress` to use more than 256MB memory and 1vCPU, even if we run this command with secified above arguments. 

## Limits per namespace
We can apply limits per namespace, below examples shows how to use policy named `LimitRange`

```yaml
apiVersion: v1
kind: LimitRange
metadata:
  name: "limits"
  namespace: limited
spec:
  limits:
    - type: "Container"
      defaultRequest:
        cpu: 0.5
        memory: 128Mi
      default:
        cpu: 1
        memory: 512Mi
```
```console
k8s-master:~$ kubectl get LimitRange --all-namespaces
No resources found
k8s-master:~$ kubectl create -f limit-resources.yaml
limitrange/limits created
k8s-master:~$ kubectl get LimitRange --all-namespaces
NAMESPACE   NAME     CREATED AT
limited     limits   2020-06-28T15:23:45Z
```
Pods spawned in `limited` namespace will inherit resource limits by default. 
```console
k8s-master:~$ kubectl get deployments.apps -n limited
NAME     READY   UP-TO-DATE   AVAILABLE   AGE
stress   1/1     1            1           9s
k8s-master:~$ kubectl get pods -n limited
NAME                      READY   STATUS    RESTARTS   AGE
stress-7cf8f4cbfb-68pk5   1/1     Running   0          13s
k8s-master:~$ kubectl describe -n limited pod stress-7cf8f4cbfb-68pk5
Name:         stress-7cf8f4cbfb-68pk5
Namespace:    limited
Priority:     0
Node:         k8s-node2.forge.local/172.16.0.12
[...]
Containers:
  stress-alpine:
    [...]
    Command:
      stress
    Args:
      --cpu
      3
      --vm-bytes
      4G
      --verbose
    State:          Running
      Started:      Sun, 28 Jun 2020 15:30:01 +0000
    Ready:          True
    Restart Count:  0
    Limits:
      cpu:     1
      memory:  512Mi
    Requests:
      cpu:        500m
      memory:     128Mi
```