# Minikube

## Installation 
<sup>(based on Fedora 32)</sup>

As a root user:

1. Download minikube binary   
```console
# curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
```
2. Make it executable
```console
# chmod +x minikube
```
3. Add minikube to PATH or move to location that already exists in PATH
```console
# install minikube /usr/local/bin/
```
4. Add repo for `kubectl` tool
```console
# cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF
```
5. Install the tool
```console
# dnf install kubectl -y
```
6. Confirm
```console
$ minikube status
```

## Configuration
If using bash auto-completion can be add as below:
```console
# kubectl completion bash > /etc/bash_completion.d/kubectl
# minikube completion bash > /etc/bash_completion.d/minikube
```
Settings can be applied to minikube cluster either by adding flag at first start or to be persisent by 
```console
$ minikube config set <property_name> <property_value>
```
Properties names are similar in both cases, below couple most common.

* The best options to run minikube on linux is to use `docker driver` (does not require virtualization) or `kvm2 driver` for virtual machine. Minikube can be also run directly (`none` driver) but this may have side effects to workstation.
```console
$ minikube start --driver=<driver_name>
```
or
```console
$ minikube config set driver <driver_name>
$ minikube start
```

* Kubernetes version 
```console
$ minikube config set kubernetes-version v1.18.0
```

