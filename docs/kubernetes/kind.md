# KIND

### Example config.yaml
```yaml
kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
nodes:
- role: control-plane
  extraPortMappings:
  - containerPort: 30000
    hostPort: 3001
    listenAddress: "0.0.0.0" 
    protocol: tcp
- role: worker
  extraPortMappings:
  - containerPort: 30000
    hostPort: 3002
- role: worker
  extraPortMappings:
  - containerPort: 30000
    hostPort: 3003
```
This will create 3 nodes cluster (master + 2 worker nodes) and map port 30000 from kind's containers to ports 3001, 3002 and 3003 on host
```console
$ kind create cluster --config kind-config.yaml --name k8s 
Creating cluster "k8s" ...
 ✓ Ensuring node image (kindest/node:v1.18.2) 🖼
 ✓ Preparing nodes 📦 📦 📦  
 ✓ Writing configuration 📜 
 ✓ Starting control-plane 🕹️ 
 ✓ Installing CNI 🔌 
 ✓ Installing StorageClass 💾 
 ✓ Joining worker nodes 🚜 
Set kubectl context to "kind-k8s"
You can now use your cluster with:

kubectl cluster-info --context kind-k8s

Thanks for using kind! 😊
$ kubectl get nodes
NAME                STATUS   ROLES    AGE     VERSION
k8s-control-plane   Ready    master   2m15s   v1.18.2
k8s-worker          Ready    <none>   99s     v1.18.2
k8s-worker2         Ready    <none>   102s    v1.18.2
$ docker ps -a
CONTAINER ID        IMAGE                  COMMAND                  CREATED              STATUS              PORTS                                                NAMES
b3d60ed336bf        kindest/node:v1.18.2   "/usr/local/bin/entr…"   About a minute ago   Up About a minute   127.0.0.1:36941->6443/tcp, 0.0.0.0:3001->30000/tcp   k8s-control-plane
694b97b99000        kindest/node:v1.18.2   "/usr/local/bin/entr…"   About a minute ago   Up About a minute   0.0.0.0:3003->30000/tcp                              k8s-worker2
d333fe701b77        kindest/node:v1.18.2   "/usr/local/bin/entr…"   About a minute ago   Up About a minute   0.0.0.0:3002->30000/tcp                              k8s-worker
```
### Extra port mapping test
To test port mapping you can create simple pod with nginx and expose svc on port 30000.
```console
$ kubectl run nginx --image=nginx 
pod/nginx created
$ kubectl expose pod nginx --type=NodePort --port=80 --target-port=80 
service/nginx exposed
$ kubectl patch svc nginx --patch '{"spec": {"ports": [{"nodePort": 30000, "port": 80, "protocol": "TCP", "targetPort": 80}]}}'
service/nginx patched
$ kubectl get svc
NAME         TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)        AGE
kubernetes   ClusterIP   10.96.0.1       <none>        443/TCP        4m34s
nginx        NodePort    10.100.94.192   <none>        80:30000/TCP   36s
$ kubectl get pods -o wide
NAME    READY   STATUS    RESTARTS   AGE     IP           NODE          NOMINATED NODE   READINESS GATES
nginx   1/1     Running   0          3m42s   10.244.2.2   k8s-worker2   <none>           <none>
$ curl http://localhost:3003
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>
[...]
```
The exposing way:

1. nginx container serves website on port 80
2. `kind` exposes port 80 of nginx's container to port 30000 of its own container (NodePort)
3. on the end `docker` publishes port 30000 of kind container (k8s-worker2 in this example) to port 3003 of the host 

### Easy clean up
```console
$ kind delete cluster --name=k8s
Deleting cluster "k8s" ...
$ docker ps -a
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
```