### Linux permissions
```sh
sudo usermod -aG wireshark $(whoami)
```

# Most useful options
- Coloring packets: View -> Coloring rules ...
- DNS resolution: View -> Name Resolution
- Additional columns: Right click on column name -> Column Preferences -> fileds: `tcp.stream` and `http.host`
- Save settings: Right click on profile -> Manage Profiles ...
- Right click on packet -> Follow -> TCP Stream
- Statistics -> Conversations

# Filters
```
http.request.method == "GET"
ip.addr == <IP> && (ip.addr == 192.168.0.0/16) || (ip.dst == <IP>)
```
# TLS
```sh
export SSLKEYLOGFILE="/path/to/log"
```
# Useful addresses
- https://virtualtotal.com
- https://any.run
- https://packettotal.com
- https://github.com/tturba/wireshark/blob/main/cheatsheet
- https://malware-traffic-analysis.net/training-exercises.html
- https://www.ettercap-project.org