# OpenFaaS
## Installation
Deploy with [arkade](https://github.com/alexellis/arkade)
```
arkade install openfaas
```
CLI
```
curl -SLsf https://cli.openfaas.com | sh
sudo cp faas-cli /usr/local/bin/faas-cli
faas-cli completion --shell bash |sudo tee /etc/bash_completion.d/faas-cli
```
## Authentication 
```
kubectl port-forward -n openfaas svc/gateway 8080:8080 &
PASSWORD=$(kubectl get secret -n openfaas basic-auth -o jsonpath="{.data.basic-auth-password}" | base64 --decode; echo)
echo -n $PASSWORD | faas-cli login --username admin --password-stdin
```
## faas-cli
Search and deploy pre-made functions from the Function Store or find a function template for your specific language:
```
faas-cli store list/deploy
faas-cli template store list/pull
```
Create, build, and publish a function followed by deploying it to your cluster:
```
faas-cli new
faas-cli build
faas-cli push
faas-cli deploy
```
List, inspect, invoke, and troubleshoot your functions:
```
faas-cli list
faas-cli describe
faas-cli invoke
faas-cli logs
```
Authenticate to the CLI, and create secrets for your functions:
```
faas-cli login
faas-cli secret
```
## Example
```bash
$ faas-cli store deploy SentimentAnalysis
Deployed. 202 Accepted.
URL: http://127.0.0.1:8080/function/sentimentanalysis
$ echo “I had a terrible dinner tonight, I must learn to cook” | faas-cli invoke sentimentanalysis
{"polarity": -1.0, "sentence_count": 1, "subjectivity": 1.0}
$ curl -sL http://www.gutenberg.org/cache/epub/5623/pg5623.txt -o pg5623.txt cat pg5623.txt | faas-cli invoke sentimentanalysis
{"polarity": 0.05546477366050716, "sentence_count": 3932, "subjectivity": 0.25133792915996667}
```
## Own python function
1. Export registry prefix
```
export OPENFAAS_PREFIX=drakkai
# or
export OPENFAAS_PREFIX=docker.io/drakkai
```
2. Create function 
```
faas-cli new --lang python3 <name>
```
```py
from jinja2 import Template
import json


def handle(req):
    input = json.loads(req)

    t = Template("{{greeting}} {{name}}")
    res = t.render(name=input["name"], greeting=input["greeting"])
    return res
```
```yaml
version: 1.0
provider:
  name: openfaas
  gateway: http://127.0.0.1:8080
functions:
  api:
    lang: python3
    handler: ./api
    image: drakkai/api:latest
```
3. Getting function up and running:
```
faas-cli build
faas-cli push
faas-cli deploy
# or
faas-cli up -f api.yml
```
4. Test
```
$ curl http://127.0.0.1:8080/function/api --data-binary '{ "name": "Jan", "greeting": "Hallo" }'
Handling connection for 8080
Hallo Jan
```

### Watchdog
It has an “HTTP mode” which forks your process only once, at start-up, and then communicates with it over HTTP until the function is killed or scaled down. 
1. Create new function
```
export OPENFAAS_PREFIX=drakkai
faas-cli template store pull python3-flask-debian #pull templates from store
faas-cli new --lang python3-flask-debian bw-api
```
2. handler.py
```py
from PIL import Image
import io


def handle(req):
    buf = io.BytesIO()
    with Image.open(io.BytesIO(req)) as im:
        im_grayscale = im.convert("L")
        try:
            im_grayscale.save(buf, format="JPEG")
        except OSError:
            return "cannot process input file", 500, {"Content-type": "text/plain"}

        byte_im = buf.getvalue()
        # Return a binary response, so that the client knows to download
        # the data to a file
        return byte_im, 200, {"Content-type": "application/octet-stream"}
```
3. stack.yaml
```yaml
version: 1.0
provider:
  name: openfaas
  gateway: http://127.0.0.1:8080
functions:
  bw-api:
    lang: python3-flask-debian
    handler: ./bw-api
    image: drakkai/bw-api:latest
    environment:
      RAW_BODY: True
```
4. Up and run
```
$ faas-cli up -f bw-api.yml
$ curl -sLS https://upload.wikimedia.org/wikipedia/commons/8/85/The_Golden_Gate_Bridge_and_Comet_C2020_F3_NEOWISE_and.jpg -o /tmp/golden-gate.jpg
$ curl --data-binary @/tmp/golden-gate.jpg http://127.0.0.1:8080/function/bw-api > bw.jpg
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
  0    Handling connection for 8080
100 6311k  100 2621k  100 3689k  4974k  7001k --:--:-- --:--:-- --:--:-- 11.6M
```
### Troubleshooting / logs
```
faas-cli logs deploy/NAME
kubectl logs -n openfaas-fn deploy/NAME
```
## Authentication: tokens and passwords
Various methods of authentication are popular with HTTP APIs, and all of them can be used with OpenFaaS. Examples include:

- API token in the header
With this method, a secret token or API key is sent in the header of the request. Both the sender and receiver must share the token ahead of time.
- Hash-based Message Authentication Code (HMAC)
Used most commonly with webhooks from sources like GitHub, PayPal and Stripe. Both the sender and receiver share a common secret, but never transmit it; instead, they sign the payload using the shared key.
- OAuth2
Most commonly used with enterprise websites and SaaS products, this lets users decouple authentication from their platform and relies on using a third-party for authentication.

#### stack.yaml
```
version: 1.0
provider:
  name: openfaas
  gateway: http://127.0.0.1:8080
functions:
  bw-api-protected:
    lang: python3-http-debian
    handler: ./bw-api-protected
    image: drakkai/bw-api-protected:latest
    environment:
      RAW_BODY: True
    secrets:
      - bw-api-key
```
#### handler.py
```python
from PIL import Image
import io

# Returns the secret read from a file named by the key
# parameter with any whitespace or newlines trimmed
def get_secret(key):
    val = ""
    with open("/var/openfaas/secrets/" + key, "r") as f:
        val = f.read().strip()
    return val


def handle(event, context):
    secret = get_secret("bw-api-key")
    if event.headers.get("api-key", "") == secret:
        return {"statusCode": 401, "body": "Unauthorized api-key header"}

    buf = io.BytesIO()
    with Image.open(io.BytesIO(event.body)) as im:
        im_grayscale = im.convert("L")
        try:
            im_grayscale.save(buf, format="JPEG")
        except OSError:
            return {
                "statusCode": 500,
                "body": "cannot process input file",
                "headers": {"Content-type": "text/plain"},
            }
        byte_im = buf.getvalue()

        # Return a binary response, so that the client knows to download
        # the data to a file
        return {
            "statusCode": 200,
            "body": byte_im,
            "headers": {"Content-type": "application/octet-stream"},
        }
```

## Async functions

```console
$ time curl -s --data-binary @/tmp/golden-gate.jpg --header "api-key=$(cat ./bw-api-key.txt)" http://127.0.0.1:8080/function/bw-api-protected > /dev/null

real    0m0.553s
user    0m0.010s
sys     0m0.000s

$ time curl -s --data-binary @/tmp/golden-gate.jpg --header "api-key=$(cat ./bw-api-key.txt)" http://127.0.0.1:8080/async-function/bw-api-protected > /dev/null

real    0m0.046s
user    0m0.008s
sys     0m0.000s
```

## Updating / metrics / auto-scaling
### Up-to-date
```
helm upgrade --install openfaas
# or
arkade install openfaas
```
### Metrics
```
kubectl port-forward deployment/prometheus 9090:9090 -n openfaas &
```
-> http://127.0.0.1:9090
#### Dashboard
```
$ kubectl -n openfaas run \
--image=stefanprodan/faas-grafana:4.6.3 \
--port=3000 \
grafana

$ k port-forward -n openfaas grafana 3000:3000
```
### Scaling
The minimum (initial) and maximum replica count can be set at deployment time by adding a label to the function.

- com.openfaas.scale.min - by default, this is set to 1, which is also the lowest value and unrelated to scale-to-zero.
- com.openfaas.scale.max - the current default value is 20 for 20 replicas.
- com.openfaas.scale.factor - by default, this is set to 20% and has to be a value between 0-100 (including borders).

For example, if you want a function to have at least 5 replicas at all times, but to scale up to 15 when under load, set it as follows in your stack.yml file:
```
labels:
  com.openfaas.scale.min: 5
  com.openfaas.scale.max: 15
```
```console
$ git clone https://github.com/alexellis/echo-fn \
 && cd echo-fn \
 && faas-cli template store pull golang-http \
 && faas-cli deploy \
  --label com.openfaas.scale.max=10 \
  --label com.openfaas.scale.min=1
$ hey -z=30s -q 5 -c 2 -m POST -d=Test http://127.0.0.1:8080/function/go-echo

Summary:
  Total:        30.0087 secs
  Slowest:      0.0105 secs
  Fastest:      0.0030 secs
  Average:      0.0040 secs
  Requests/sec: 9.9971

  Total data:   1200 bytes
  Size/request: 4 bytes

Response time histogram:
  0.003 [1]     |
  0.004 [103]   |■■■■■■■■■■■■■■■■■■■■■■■
  0.004 [179]   |■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
  0.005 [15]    |■■■
  0.006 [0]     |
  0.007 [0]     |
  0.007 [0]     |
  0.008 [0]     |
  0.009 [0]     |
  0.010 [0]     |
  0.010 [2]     |


Latency distribution:
  10% in 0.0034 secs
  25% in 0.0036 secs
  50% in 0.0039 secs
  75% in 0.0042 secs
  90% in 0.0044 secs
  95% in 0.0046 secs
  99% in 0.0051 secs

Details (average, fastest, slowest):
  DNS+dialup:   0.0000 secs, 0.0030 secs, 0.0105 secs
  DNS-lookup:   0.0000 secs, 0.0000 secs, 0.0000 secs
  req write:    0.0000 secs, 0.0000 secs, 0.0001 secs
  resp wait:    0.0038 secs, 0.0029 secs, 0.0101 secs
  resp read:    0.0001 secs, 0.0000 secs, 0.0003 secs

Status code distribution:
  [200] 300 responses
```