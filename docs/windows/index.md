# Windows
## Networking
Add/remove DNS servers from virtual card
```powershell
Get-NetAdapter "vEthernet (NAT Switch)" |Set-DnsClientServerAddress -ServerAddresses 172.16.0.10
Get-NetAdapter "vEthernet (NAT Switch)" |Set-DnsClientServerAddress -ResetServerAddresses
```
### WSL2
Connect from WSL to port (`6443`) on Windows localhost
```powershell
netsh interface portproxy add v4tov4 listenport=6443 listenaddress=0.0.0.0 connectport=6443 connectaddress=127.0.0.1
New-NetFirewallRule -DisplayName "CRC API from WSL2" -Direction Inbound -LocalPort 6443 -Protocol TCP -Action Allow -RemoteAddress 172.16.0.0/12
```
Revert above changes
```powershell
netsh interface portproxy delete v4tov4 listenport=6443 listenaddress=0.0.0.0
Remove-NetFirewallRule -DisplayName "CRC API from WSL2"
```
Enable forwarding for virtual switch, in this case `WSL`, `NAT Switch` and `Default Switch`
```powershell
Get-NetIPInterface | where { $_.InterfaceAlias -eq 'vEthernet (WSL)' -or $_.InterfaceAlias -eq 'vEthernet (NAT Switch)' -or $_.InterfaceAlias -eq 'vEthernet (Default Switch)' } | Set-NetIPInterface -Forwarding Enabled
```

## Hyper-V
Remove virtual machine firmware file
```powershell
$VMName = <virtual machine name>
Get-VMFirmware -VMName $VMName |ForEach-Object {Set-VMFirmware -BootOrder ($_.Bootorder | Where-Object {$_.BootType -ne 'File'}) $_ }
```