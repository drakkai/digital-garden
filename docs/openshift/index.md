# OpenShift 4
## Etcd backup
```console
oc debug node/ocp-control-plane-1.lab.forge.local
chroot /host
/usr/local/bin/cluster-backup.sh /home/core/assets/backup
```
or 
```console
oc debug node/ocp-control-plane-1.lab.forge.local -- chroot /host /usr/local/bin/cluster-backup.sh /home/core/assets/backup
```
## Shut down cluster gracefully
```console
for node in $(oc get nodes -o jsonpath='{.items[*].metadata.name}'); do oc debug node/${node} -- chroot /host shutdown -h 1; done 
```