# Single-node OpenShift
## Manual installation
[Requirements](https://docs.openshift.com/container-platform/4.11/installing/installing_sno/install-sno-preparing-to-install-sno.html#install-sno-requirements-for-installing-on-a-single-node_install-sno-preparing) -> [example preparation](https://itnext.io/okd-4-5-single-node-cluster-on-windows-10-using-hyper-v-3ffb7b369245)

Set variables
```console
OCP_VERSION="latest-4.11"
ARCH="x86_64"
RHCOS_ISO=<path_to_ISO>
```
Download binaries
```console
curl -k https://mirror.openshift.com/pub/openshift-v4/clients/ocp/$OCP_VERSION/openshift-client-linux.tar.gz -o oc.tar.gz
curl -k https://mirror.openshift.com/pub/openshift-v4/clients/ocp/$OCP_VERSION/openshift-install-linux.tar.gz -o openshift-install-linux.tar.gz
curl -k https://mirror.openshift.com/pub/openshift-v4/clients/coreos-installer/latest/coreos-installer -o coreos-installer
```
Download RHCOS ISO
```console
ISO_URL=$(./openshift-install coreos print-stream-json | grep location | grep $ARCH | grep iso | cut -d\" -f4)
curl -L $ISO_URL -o $RHCOS_ISO
```
Prepare config `install-config.yaml`
```yaml
apiVersion: v1
baseDomain: forge.local
compute:
  - name: worker
    replicas: 0
controlPlane:
  name: master
  replicas: 1
metadata:
  name: lab
networking:
  networkType: OVNKubernetes
  clusterNetwork:
    - cidr: 10.128.0.0/14
      hostPrefix: 23
  serviceNetwork:
    - 172.30.0.0/16
platform:
  none: {}
bootstrapInPlace:
  installationDisk: '/dev/sda'
pullSecret: <pull_secret>
sshKey: |
  <ssh_key>
```
[pull secret](https://console.redhat.com/openshift/install/pull-secret)

Generate OCP assets
```console
cp install-config.yaml sno/
./openshift-install --dir=sno/ create single-node-ignition-config
```
Embed ign file to ISO
```console
# for 4.10
cp sno/bootstrap-in-place-for-live-iso.ign sno/iso.ig
./coreos-installer iso ignition embed -fi sno/iso.ign $RHCOS_ISO
# for 4.11+
./coreos-installer iso ignition embed -fi sno/bootstrap-in-place-for-live-iso.ign $RHCOS_ISO
```

Attach ISO to host and boot the server with it. Monitor installation proces:
```console
./openshift-install --dir=sno wait-for install-complete
./openshift-install --dir=sno wait-for bootstrap-complete --log-level=debug
```
or directly from control-plane:
```console
ssh core@<control_plane_ip>
journalctl -b -f -u release-image.service -u bootkube.service
```
**_NOTE:_** In hyper-v you have to change the boot order (iso> disk) right after the first boot otherwise the bootstrap process will continue indefinitely. 